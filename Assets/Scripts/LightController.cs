﻿using UnityEngine;
using System.Collections;
using System;

public class LightController : MonoBehaviour
{
    public AudioClip turnOnSound;
    
    public float turnOnSoundVolume = .5F;

		public AudioClip allOffSound;
		public float allOffSoundVolume = .5F;

    public float delay = 1F;

    private InputController input;
    
    private GameObject[] lights;
    
    private GameObject currentLight = null;
    
    private bool busy = false;
    
    void Awake()
    {
        input = GameObject.FindGameObjectWithTag("Input").GetComponent<InputController>();
        lights = GameObject.FindGameObjectsWithTag("Light");
        
        Array.Sort(lights, delegate(GameObject a, GameObject b)
        {
            return a.name.CompareTo(b.name);
        });
        
        foreach (var light in lights)
        {
            TurnOff(light);
        }
    }
    
    void FixedUpdate()
    {
        if (busy)
        {
            return;
        }
    
        if (input.button != ButtonType.None && input.button != ButtonType.Fire)
        {
            int lightIndex;
        
            switch (input.button)
            {
                case ButtonType.Light1:
                    lightIndex = 0;
                    break;
                
                case ButtonType.Light2:
                    lightIndex = 1;
                    break;
                
                case ButtonType.Light3:
                    lightIndex = 2;
                    break;
                
                case ButtonType.Light4:
                    lightIndex = 3;
                    break;
                
                case ButtonType.Light5:
                    lightIndex = 4;
                    break;
                
                default:
                    lightIndex = 99;
                    break;
            }
            
            if (lightIndex < lights.Length)
            {
                GameObject newLight = lights[lightIndex];
                
                if (currentLight != null && currentLight != newLight)
                {
                    TurnOff(currentLight);
                }
                
                currentLight = newLight;
                busy = true;
                StartCoroutine(TurnOn(currentLight));
            }
            else
            {
                if (currentLight != null)
                {
                    TurnOff(currentLight);
                    currentLight = null;
										AudioManager.Instance.PlayClip(allOffSound, allOffSoundVolume);
                }
            }
        }
    }
    
    IEnumerator TurnOn(GameObject light)
    {
        yield return new WaitForSeconds(delay);
        AudioManager.Instance.PlayClip(turnOnSound, turnOnSoundVolume);
        
        foreach (Transform child in light.transform)
        {
            child.gameObject.light.enabled = true;
        }
        
        busy = false;
    }
    
    void TurnOff(GameObject light)
    {
        foreach (Transform child in light.transform)
        {
            child.gameObject.light.enabled = false;
        }
    }
}
