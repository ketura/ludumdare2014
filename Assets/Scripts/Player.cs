﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
	protected GameController GC;


	// Use this for initialization
	void Start () 
	{
		GC = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	private bool isTriggered = false;
	public void OnTriggerEnter(Collider other)
	{
		if (!isTriggered)
		{
			Snowman snowman = other.gameObject.GetComponent<Snowman>();
			if (snowman != null)
			{
				isTriggered = true;
				StartCoroutine(DoImpact());
			}
		}
	}

	IEnumerator DoImpact()
	{
		audio.Play();
		GC.Lose();

		while (audio.isPlaying)
		{
			yield return null;
		}
	}
}
