﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour 
{
	public GameObject HidingSpot;
	public GameObject MovingSpot;

	public float WaitTime;
	public float DarkSpawnChance;

	public int Level;

	public enum WaypointType { HidingPlace, WalkingTarget };
	public WaypointType Type;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
