﻿using UnityEngine;
using System.Collections;

public class SnowmanBody : MonoBehaviour {

	public float TimeToDespawn = 3;
	// Use this for initialization
	void Start () 
	{
		StartCoroutine(Despawn(TimeToDespawn));
	}

	public IEnumerator Despawn(float time)
	{
		yield return new WaitForSeconds(time);

		Destroy(gameObject);
	}
}
