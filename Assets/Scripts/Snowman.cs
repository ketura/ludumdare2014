﻿using UnityEngine;
using System.Collections;

public class Snowman : MonoBehaviour 
{
	public int HP;

	public float Speed;

	public int CurrentLevel;

	public GameObject GibletPrefab;
	public GameObject Model;

	public enum SnowmanAction { Walking, Hiding, Dying, Finished };
	public SnowmanAction CurrentAction;

	public Waypoint CurrentTarget;

	public AudioClip Death1;
	public AudioClip Death2;
	public AudioClip Move1;
	public AudioClip Move2;
	public AudioClip Move3;
	public AudioClip Moan;

	public float MoanChance;
	public float MoanFreq;


	protected SnowmanSpawner Spawner;
	protected WaypointController WP;
	protected GameController GC;

	private bool failsafe = false;

	// Use this for initialization
	void Start () 
	{
		Spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<SnowmanSpawner>();
		WP = GameObject.FindGameObjectWithTag("WaypointController").GetComponent<WaypointController>();
		GC = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

		if(HP <= 0)
			HP = 1;

		if (CurrentLevel < 0)
			CurrentLevel = 0;

		CurrentAction = SnowmanAction.Hiding;

		float speedfactor = Random.Range(0.5f, 1.5f) + GC.Difficulty * GC.SpeedFactor;
		Speed *= speedfactor;

		StartCoroutine(TryMoan());
	}
	
	// Update is called once per frame
	void Update () 
	{
		switch (CurrentAction)
		{
			case SnowmanAction.Walking:
				StartCoroutine(WalkToTarget(Speed * GC.Difficulty * GC.SpeedFactor));
				break;

			case SnowmanAction.Hiding:
				StartCoroutine(Hide());
				break;

			case SnowmanAction.Dying:
				StartCoroutine(BreakApart());
				break;
		}
	}

	private bool moaning = false;
	public IEnumerator TryMoan()
	{
		if(!moaning)
		{
			moaning = true;

			while (true)
			{
				yield return new WaitForSeconds(MoanFreq * Random.Range(0.8f, 1.2f));

				float dice = Random.Range(0, 1);
				if (failsafe || dice <= MoanChance)
				{
					audio.clip = Moan;
					audio.Play();
					yield break;
				}
			}
		}

	}

	private bool breaking = false;
	public IEnumerator BreakApart()
	{
		if (!breaking)
		{
			breaking = true;

			bool coin = Random.Range(0f, 1.0f) >= 0.5f;
			if (coin)
				audio.clip = Death1;
			else
				audio.clip = Death2;

			audio.Play();
			Model.SetActive(false);
			Instantiate(GibletPrefab, transform.position, Quaternion.identity);

			while (audio.isPlaying)
				yield return null;
			
			Destroy(gameObject);

			

			breaking = false;
		}


	}

	private bool walking = false;
	public IEnumerator WalkToTarget(float speed)
	{
		if (!walking)
		{
			walking = true;

			float result = Random.Range(0, 3);

			if(result >=2)
				audio.clip = Move1;
			else if (result >= 1)
				audio.clip = Move2;
			else
				audio.clip = Move3;

			audio.Play();

			Vector3 origin = transform.position;
			float time = 0;
			while (time < 1.0f)
			{
				if (failsafe)
				{
					walking = false;
					yield break;
				}

				time += Time.deltaTime * speed;
				transform.position = Vector3.Lerp(origin, CurrentTarget.HidingSpot.transform.position, time);
				yield return null;
			}

			if(!failsafe)
				CurrentAction = SnowmanAction.Hiding;

			walking = false;
		}

		yield return null;
	}

	private bool hiding = false;
	public IEnumerator Hide()
	{
		if (!hiding)
		{
			if (CurrentLevel == 8)
			{
				if (GetNewTarget())
					if (!failsafe)
						CurrentAction = SnowmanAction.Walking;
				yield break;
			}

			hiding = true;
			float time;
			if (CurrentTarget == null)
				time = 0;
			else
				time = Random.Range(1.0f, CurrentTarget.WaitTime);

			
			yield return new WaitForSeconds(time);

			if(GetNewTarget())
				if(!failsafe)
					CurrentAction = SnowmanAction.Walking;


			hiding = false;
		}
		yield return null;
	}

	private bool GetNewTarget()
	{
		CurrentTarget = WP.GetNextWaypoint(CurrentLevel);
		if (CurrentTarget == null)
		{
			CurrentAction = SnowmanAction.Finished;
			failsafe = true;
			return false;
		}
		else
		{
			CurrentLevel = CurrentTarget.Level;
			return true;
		}
	}

	public void Kill()
	{
		failsafe = true;
		Spawner.KillSnowman(this);
		CurrentAction = SnowmanAction.Dying;
	}

	public void Hit()
	{
		HP -= 1;
		if (HP <= 0)
			Kill();
	}
}
