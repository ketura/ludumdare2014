﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour
{
    public void OnPlay()
    {
        Application.LoadLevel("Game");
    }
    
    public void OnQuit()
    {
        Application.Quit();
    }
}
