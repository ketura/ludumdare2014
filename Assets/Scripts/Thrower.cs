﻿using UnityEngine;
using System.Collections;

public class Thrower : MonoBehaviour {
    public GameObject snowball;
    
    public AudioClip throwSound;

    public float speed = 10F;

    public float cooldown = 1F;

    private GameController game;

    private InputController input;
    
    private bool firing = false;
    
    void Awake()
    {
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        input = GameObject.FindGameObjectWithTag("Input").GetComponent<InputController>();
    }
    
    void FixedUpdate()
    {
        if (input.button == ButtonType.Fire && !firing)
        {
            if (game.snowballCount > 0)
            {
                game.snowballCount--;
                StartCoroutine(Fire());
            }
        }
    }
    
    IEnumerator Fire()
    {
        firing = true;
        AudioManager.Instance.PlayClip(throwSound);
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var shot = Instantiate(snowball, transform.position, Quaternion.identity) as GameObject;
        shot.rigidbody.velocity = ray.direction.normalized * speed;
        yield return new WaitForSeconds(cooldown);
        firing = false;
    }
}
