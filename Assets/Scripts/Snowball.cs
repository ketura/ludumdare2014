﻿using UnityEngine;
using System.Collections;

public class Snowball : MonoBehaviour
{
	public AudioClip StandardHit;
	public AudioClip SnowmanHit;

	public AudioSource ThisSource;
    
    private bool isTriggered = false;

	void OnTriggerEnter(Collider other)
	{
		if (!isTriggered)
		{
			PlayerBubble pb = other.gameObject.GetComponent<PlayerBubble>();
			if (pb != null)
				return;

			isTriggered = true;
      
			Snowman snowman = other.gameObject.GetComponent<Snowman>();
			if (snowman != null)
			{
				audio.clip = SnowmanHit;
				snowman.Hit();
			}
			else
			{
				audio.clip = StandardHit;
			}
    
				StartCoroutine(DoImpact());
			}
	}

	void FixedUpdate()
	{
		if (transform.position.y < -10)
		{
			Destroy(gameObject);
		}
	}

	IEnumerator DoImpact()
	{
		audio.Play();

		while (audio.isPlaying)
		{
			yield return null;
		}

		Destroy(gameObject);
	}
}
