﻿using UnityEngine;
using System.Collections;

public enum ButtonType
{
    None,
    Fire,
    Pack,
    Light1,
    Light2,
    Light3,
    Light4,
    Light5,
    Quit
}

public class InputController : MonoBehaviour
{
    public ButtonType button;
    
	void Start()
	{
	}
    
	void FixedUpdate()
	{
		if (Input.GetButton("Quit"))
        {
            button = ButtonType.Quit;
        }
        else if (Input.GetButton("Fire1"))
        {
            button = ButtonType.Fire;
        }
        else if (Input.GetButton("Pack"))
        {
            button = ButtonType.Pack;
        }
        else if (Input.GetButton("Light1"))
        {
            button = ButtonType.Light1;
        }
        else if (Input.GetButton("Light2"))
        {
            button = ButtonType.Light2;
        }
        else if (Input.GetButton("Light3"))
        {
            button = ButtonType.Light3;
        }
        else if (Input.GetButton("Light4"))
        {
            button = ButtonType.Light4;
        }
        else if (Input.GetButton("Light5"))
        {
            button = ButtonType.Light5;
        }
        else
        {
            button = ButtonType.None;
        }
	}
}
