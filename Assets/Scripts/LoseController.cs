﻿using UnityEngine;
using System.Collections;

public class LoseController : MonoBehaviour
{
    public void OnPlayAgain()
    {
        Application.LoadLevel("Game");
    }
    
    public void OnMainMenu()
    {
        Application.LoadLevel("MainMenu");
    }
}
