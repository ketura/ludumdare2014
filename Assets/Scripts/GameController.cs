﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour 
{
	public int Difficulty;
	public int Rounds;
	public float SpeedFactor;
    
    public AudioClip gong;
    public float gongVolume = 1F;
    public int morningHour = 6;
    public float hourDuration = 60F;
    public int hour = 0;
    
    public AudioClip packSound;
    public float packRate = .2F;
    
    public int snowballCount;
    public float packed = 0F;
    
    private InputController input;
    
    private GameObject packSoundPlayer = null;
    private Slider packingProgress;
    private Text showballCountText;

    void Awake()
    {
        input = GameObject.FindGameObjectWithTag("Input").GetComponent<InputController>();
        InitPackSoundPlayer();
        packingProgress = GameObject.FindGameObjectWithTag("PackingProgress").GetComponent<Slider>();
        showballCountText = GameObject.FindGameObjectWithTag("SnowballCount").GetComponent<Text>();
        
        StartCoroutine(RunClock());
    }

	// Use this for initialization
	void Start () 
	{

	}
    
    void FixedUpdate()
    {
        if (input.button == ButtonType.Quit)
        {
            Application.LoadLevel("MainMenu");
        }
    
        showballCountText.text = snowballCount.ToString();
        Pack();
    }
	
	// Update is called once per frame
	void Update () 
	{
	}
    
    void Pack()
    {
        packingProgress.value = packed;
    
        if (input.button == ButtonType.Pack)
        {
            if (!packSoundPlayer.audio.isPlaying)
            {
                packSoundPlayer.audio.Play();
            }
        
            packed += Time.fixedDeltaTime * packRate;
            
            if (packed > 1)
            {
                snowballCount++;
                packed = 0F;
                packSoundPlayer.audio.Stop();
            }
        }
        else
        {
            packed = 0F;
            packSoundPlayer.audio.Stop();
        }
    }
    
    void InitPackSoundPlayer()
    {
        packSoundPlayer = new GameObject();
        var audioSource = packSoundPlayer.AddComponent<AudioSource>();
        audioSource.audio.clip = packSound;
        audioSource.loop = true;
        packSoundPlayer.transform.parent = transform;
    }
    
    IEnumerator RunClock()
    {
        for (hour = 0; hour <= morningHour; ++hour)
        {
            StartCoroutine(TellTime());
            Difficulty = hour + 1;
            
            if (hour < morningHour)
            {
                yield return new WaitForSeconds(hourDuration);
            }
        }
        
        Application.LoadLevel("Victory");
    }
    
    IEnumerator TellTime()
    {
        if (hour > 0 && hour < morningHour)
        {
            for (int i = 1; i <= hour; ++i)
            {
                AudioManager.Instance.PlayClip(gong, gongVolume);
                yield return new WaitForSeconds(1F);
            }
        }
    }

    public void Lose()
    {
        Debug.Log("You Lose");
        Application.LoadLevel("Lose");
    }
}
