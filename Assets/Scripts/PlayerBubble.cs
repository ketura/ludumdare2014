﻿using UnityEngine;
using System.Collections;

public class PlayerBubble : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	private bool isTriggered = false;
	public void OnTriggerEnter(Collider other)
	{
		Snowman snowman = other.gameObject.GetComponent<Snowman>();
		if (snowman != null)
		{
			isTriggered = true;
			StartCoroutine(DoImpact());
		}			
	}

	IEnumerator DoImpact()
	{
		audio.Play();

		while (audio.isPlaying)
		{
			yield return null;
		}
	}
}
