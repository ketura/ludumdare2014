﻿using UnityEngine;
using System.Collections;

public class AudioManager : Singleton<AudioManager>
{
    void FixedUpdate()
    {
        foreach (Transform child in transform)
        {
            if (!child.audio.isPlaying)
            {
                Destroy(child.gameObject);
            }
        }
    }

    public void PlayClip(AudioClip clip)
    {
        PlayClip(clip, 1F);
    }
    
    public void PlayClip(AudioClip clip, float volume)
    {
        var childObject = new GameObject();
        var audioSource = childObject.AddComponent<AudioSource>();
        audioSource.audio.clip = clip;
        audioSource.audio.volume = volume;
        childObject.audio.Play();
        childObject.transform.parent = transform;
    }
}
