﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SnowmanSpawner : MonoBehaviour 
{
	public GameObject SnowmanPrefab;
	
	public int MaxSnowmen;

	public bool SpawnActive;

	public int Difficulty;
	public int DifficultySpawnFactor;
	public float SpawnInterval;

	public float FastSpawnMultiplier;
	public float SlowSpawnMultiplier;
	public float DifficultyDurationFactor;

	public float FastSpawnFrequency;
	public float SlowSpawnFrequency;
	public float ModeChangeInterval;

	public enum SpawnMode { Normal, Fast, Slow };

	public SpawnMode CurrentMode;

	public Vector2 XSpawnCoord;
	public Vector2 ZSpawnCoord;
	public float YSpawnCoord;

	protected List<Snowman> Snowmen;
	protected int SnowmenCount;
	protected GameController GC;



	// Use this for initialization
	void Start () 
	{
		if (MaxSnowmen < 0)
			MaxSnowmen = 0;

		CurrentMode = SpawnMode.Normal;


		Snowmen = new List<Snowman>();
		GC = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		Difficulty = GC.Difficulty;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (SpawnActive)
		{
			SpawnSnowmen();
		}
	}

	public void KillSnowman(Snowman dead)
	{
		Snowmen.Remove(dead);
		SnowmenCount = Snowmen.Count;
	}

	protected void SpawnSnowmen()
	{
		
		if(SnowmenCount < (float)MaxSnowmen * (Difficulty * DifficultySpawnFactor))
		{
			float delay = SpawnInterval / (Difficulty * DifficultyDurationFactor);
			StartCoroutine(SpawnDelay(SpawnInterval));
		}
	}

	private bool spawning = false;

	public IEnumerator SpawnDelay(float delay)
	{
		if (!spawning)
		{
			spawning = true;

			yield return new WaitForSeconds(delay);

			Vector3 pos = new Vector3(Random.Range(XSpawnCoord.x, XSpawnCoord.y), YSpawnCoord, Random.Range(ZSpawnCoord.x, ZSpawnCoord.y));

			Snowman snowman = ((GameObject)Instantiate(SnowmanPrefab, pos, Quaternion.identity)).GetComponent<Snowman>();

			SnowmenCount++;

			if (GC.hour < 1)
			{
				snowman.CurrentLevel = 3;
			}

			spawning = false;
		}

		yield return null;
	}

	private bool changingMode = false;
	public IEnumerator ChangeMode()
	{
		if (!changingMode)
		{
			yield return null;
		}

		yield return null;
	}

	public void NewWave()
	{
		SpawnActive = true;
		Difficulty = GC.Difficulty;
	}

	public void EndWave()
	{
		SpawnActive = false;

		List<Snowman> CurrentSnowmen = new List<Snowman>(Snowmen);
		foreach (Snowman snowman in CurrentSnowmen)
		{
			snowman.Kill();
		}
	}
}
