﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaypointController : MonoBehaviour 
{

	Dictionary<int, List<Waypoint>> Waypoints;
	public Waypoint StartPoint;
	public Waypoint EndPoint;

	// Use this for initialization
	void Start () 
	{
		Waypoints = new Dictionary<int, List<Waypoint>>();
		foreach (GameObject go in GameObject.FindGameObjectsWithTag("Tree"))
		{
			Waypoint wp = go.GetComponent<Waypoint>();

			if (!Waypoints.ContainsKey(wp.Level))
				Waypoints[wp.Level] = new List<Waypoint>();

			Waypoints[wp.Level].Add(wp);
		}

		Waypoints[EndPoint.Level] = new List<Waypoint>();
		Waypoints[EndPoint.Level].Add(EndPoint);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public Waypoint GetNextWaypoint(int currentLevel)
	{
		currentLevel++;
		if (!Waypoints.ContainsKey(currentLevel))
		{
			return EndPoint;
		}

		if (Waypoints[currentLevel].Count == 1)
			return Waypoints[currentLevel][0];
		else
		{
			int index = Random.Range(0, Waypoints[currentLevel].Count);
			return Waypoints[currentLevel][index];
		}

	}
}
